import React from 'react'
import { useRecoilValue } from 'recoil'
import PropTypes from 'prop-types'
import revenueModelsData from '../RevenueModelExplorer/revenueModelsData'
import revenueModelAssessmentsAtom from '../../../recoil/data/revenueModelAssessments'
import selectedRevenueModelIdsAtom from '../../../recoil/data/selectedRevenueModelIds'
import RevenueModelAssessorCheckbox from './RevenueModelAssessorCheckbox'
import styles from './RevenueModelAssessor.module.css'

function getRevenueModels(revenueModelIds) {
	const revenueModels = []

	Object.values(revenueModelsData).forEach((revenueModelCategory) => {
		revenueModelCategory.revenueModels.forEach((revenueModel) => {
			if (revenueModelIds.includes(revenueModel.id)) {
				revenueModels.push(revenueModel)
			}
		})
	})

	return revenueModels
}

function RevenueModelAssessorQuadrant({ quadrant }) {
	const revenueModelAssessments = useRecoilValue(revenueModelAssessmentsAtom)
	const selectedRevenueModelIds = useRecoilValue(selectedRevenueModelIdsAtom)

	const assessedRevenueModelIds = Object.keys(revenueModelAssessments)

	const revenueModelIdsToRender = selectedRevenueModelIds.filter((revenueModelId) => (
		!assessedRevenueModelIds.includes(revenueModelId)
		|| revenueModelAssessments[revenueModelId] === quadrant
	))

	const revenueModelsToRender = getRevenueModels(revenueModelIdsToRender)

	return (
		<div className={`
			${styles.quadrant}
			${styles[`quadrant--${quadrant}`]}
		`}
		>
			<div className={styles.revenueModels}>
				{revenueModelsToRender.map((revenueModel) => (
					<RevenueModelAssessorCheckbox
						key={revenueModel.id}
						revenueModel={revenueModel}
						quadrant={quadrant}
						checked={revenueModelAssessments[revenueModel.id] === quadrant}
					/>
				))}
			</div>
		</div>
	)
}

RevenueModelAssessorQuadrant.propTypes = {
	quadrant: PropTypes.string.isRequired,
}

export default RevenueModelAssessorQuadrant
