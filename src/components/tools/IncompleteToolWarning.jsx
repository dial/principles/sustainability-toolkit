import React from 'react'

function IncompleteToolWarning() {
	return (
		<div className="alert alert--warning">
			This tool is still being developed and will be added to the site very soon.
			In the meantime, please continue reading through the guide.
		</div>
	)
}

export default IncompleteToolWarning
