import { v4 as uuidv4 } from 'uuid'

export function generateCompetitor(index) {
	return {
		key: uuidv4(),
		label: `Competitor ${index + 1}`,
	}
}

export function generateCompetitors(count) {
	return Array.from(Array(count), (_, index) => generateCompetitor(index))
}
