import React, { useCallback } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
} from '@mui/material'
import toolStyles from '../Tools.modules.css'
import { replaceKeyedObject } from '../../../utils'
import competitorsAtom from '../../../recoil/data/competitors'
import productComponentsAtom from '../../../recoil/data/productComponents'
import nextProductComponentIdAtom from '../../../recoil/data/nextProductComponentId'
import componentComparisonsAtom, {
	throughComponentAndCompetitorStructure,
} from '../../../recoil/data/componentComparisons'
import { generateProductComponent } from '../CoreModularHackable/utils'
import ComponentComparisonHead from './ComponentComparisonHead'
import ComponentComparisonBody from './ComponentComparisonBody'
import styles from './ComponentComparison.module.css'

function getComponentComparisonHash(componentComparison) {
	return `${componentComparison.productComponentKey}_${componentComparison.competitorKey}`
}

function ComponentComparison() {
	const competitors = useRecoilValue(competitorsAtom)
	const setCompetitors = useSetRecoilState(competitorsAtom)
	const productComponents = useRecoilValue(productComponentsAtom)
	const setProductComponents = useSetRecoilState(productComponentsAtom)
	const componentComparisons = useRecoilValue(throughComponentAndCompetitorStructure({
		productComponentOrderByKey: productComponents.map((productComponent) => productComponent.key),
		competitorOrderByKey: competitors.map((competitor) => competitor.key),
	}))
	const setComponentComparisonsHashMap = useSetRecoilState(componentComparisonsAtom)
	const nextProductComponentId = useRecoilValue(nextProductComponentIdAtom)
	const setNextProductComponentId = useSetRecoilState(nextProductComponentIdAtom)

	const updateCompetitor = useCallback((competitor) => {
		setCompetitors((previousCompetitors) => (
			replaceKeyedObject(competitor, previousCompetitors)
		))
	}, [])

	const updateProductComponent = useCallback((productComponent) => {
		setProductComponents((previousProductComponents) => (
			replaceKeyedObject(productComponent, previousProductComponents)
		))
	}, [])

	const updateComponentComparison = useCallback((
		competitorIndex,
		productComponentKey,
		comparison,
	) => {
		const competitor = competitors[competitorIndex]
		const componentComparison = {
			competitorKey: competitor.key,
			productComponentKey,
			comparison,
		}
		setComponentComparisonsHashMap((previousComponentComparisonHashMap) => {
			const newComponentComparisonHashMap = {
				...previousComponentComparisonHashMap,
			}
			const componentComparisonHash = getComponentComparisonHash(componentComparison)
			newComponentComparisonHashMap[componentComparisonHash] = componentComparison

			return newComponentComparisonHashMap
		})
		// Recoil will re-generate the competitors every page load if they haven't
		// been saved.  This means we need to save the competitors if component
		// comparisons have been edited (since we rely on the competitor key)
		setCompetitors((previousCompetitors) => previousCompetitors)
		setProductComponents((previousProductComponents) => previousProductComponents)
	}, [])

	const incrementNextProductComponentId = (incrementAmount = 1) => {
		setNextProductComponentId(nextProductComponentId + incrementAmount)
	}

	const removeProductComponent = useCallback((productComponent) => {
		setProductComponents((previousProductComponents) => (
			previousProductComponents.filter((item) => item.key !== productComponent.key)
		))
	}, [])

	const addProductComponentWithType = useCallback((nowType) => {
		const newProductComponent = generateProductComponent(nextProductComponentId)
		newProductComponent.now = nowType
		incrementNextProductComponentId()
		setProductComponents((previousProductComponents) => [
			...previousProductComponents,
			newProductComponent,
		])
	}, [])

	return (
		<div className={toolStyles.interactive}>
			<TableContainer>
				<Table aria-label="Component Comparison Tool" className={styles.componentComparisonTable}>
					<ComponentComparisonHead
						competitors={competitors}
						updateCompetitor={updateCompetitor}
					/>
					<ComponentComparisonBody
						competitors={competitors}
						productComponents={productComponents}
						componentComparisons={componentComparisons}
						updateComponentComparison={updateComponentComparison}
						updateProductComponent={updateProductComponent}
						removeProductComponent={removeProductComponent}
						addProductComponentWithType={addProductComponentWithType}
					/>
				</Table>
			</TableContainer>
			<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>
		</div>
	)
}

export default ComponentComparison
