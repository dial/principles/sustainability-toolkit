import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
} from '@mui/material'
import styles from './Tools.modules.css'

function InnerTableAlertRow({ colSpan, children }) {
	return (
		<TableRow>
			<TableCell colSpan={colSpan}>
				<div className={`
						alert
						alert--warning
						${styles.innerTableAlert}
					`}
				>
					{children}
				</div>
			</TableCell>
		</TableRow>
	)
}

InnerTableAlertRow.propTypes = {
	colSpan: PropTypes.number.isRequired,
	children: PropTypes.node.isRequired,
}

export default InnerTableAlertRow
