import React from 'react'
import { useRecoilValue } from 'recoil'
import selectedCostReductionModelIdsAtom from '../../../recoil/data/selectedCostReductionModelIds'
import styles from './CostReductionModelExplorerUserDeck.module.css'

function CostReductionModelExplorerUserDeck() {
	const selectedCostReductionModelIds = useRecoilValue(selectedCostReductionModelIdsAtom)

	const costReductionModelsCount = selectedCostReductionModelIds.length

	return (
		<div className={styles.userDeck}>
			{`You’ve selected ${costReductionModelsCount} cost reduction model${costReductionModelsCount !== 1 ? 's' : ''}.`}
		</div>
	)
}

export default CostReductionModelExplorerUserDeck
