import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
} from '@mui/material'
import {
	ACTIVITY_STAGE,
	ACTIVITY_CHANNEL_DISPLAY_ORDER,
} from '../../../constants'
import styles from '../Tools.modules.css'
import SyncedTextAreaAutoresizeSet from '../../generic/SyncedTextareaAutoresizeSet'

function getChannelByDisplayIndex(index) {
	if (ACTIVITY_CHANNEL_DISPLAY_ORDER.length < index) {
		return ''
	}

	return ACTIVITY_CHANNEL_DISPLAY_ORDER[index]
}

function getStageLabel(stage) {
	switch (stage) {
	case ACTIVITY_STAGE.AFTER_SALES:
		return 'After-Sales'
	case ACTIVITY_STAGE.AWARENESS:
		return 'Awareness'
	case ACTIVITY_STAGE.DELIVERY:
		return 'Delivery'
	case ACTIVITY_STAGE.EVALUATION:
		return 'Evaluation'
	case ACTIVITY_STAGE.PURCHASE:
		return 'Purchase'
	default:
		return ''
	}
}

function ChannelStrategyMixEditorRow({
	stage,
	channelActivitiesForStage,
	updateChannelActivity,
}) {
	const handleUpdatedTextValue = (value, index) => {
		const channel = getChannelByDisplayIndex(index)
		updateChannelActivity({
			channel,
			stage,
			activity: value,
		})
	}

	return (
		<TableRow>
			<TableCell
				component="th"
				scope="row"
				className={styles.rotatedCell}
			>
				<div className={styles.rotatedCellInner}>
					{getStageLabel(stage)}
				</div>
			</TableCell>
			<SyncedTextAreaAutoresizeSet
				textValues={channelActivitiesForStage}
				updateTextValue={handleUpdatedTextValue}
				minRows={5}
				wrapper={TableCell}
			/>
		</TableRow>
	)
}

ChannelStrategyMixEditorRow.propTypes = {
	stage: PropTypes.string.isRequired,
	channelActivitiesForStage: PropTypes.arrayOf(PropTypes.string).isRequired,
	updateChannelActivity: PropTypes.func.isRequired,
}

export default ChannelStrategyMixEditorRow
