import React from 'react'
import PropTypes from 'prop-types'
import { DownloadIcon } from '@heroicons/react/outline'
import styles from '../Tools.modules.css'

function CodificationEditorFooter({
	showDownloadButton,
}) {
	return showDownloadButton ? (
		<div className={`
			${styles.controls}
			${styles['controls--download-only']}
		`}
		>
			<button
				className={`
					button
					button--link
					button--with-icon
					${styles.controlButton}
				`}
				tabIndex="0"
				type="button"
				aria-label="download"
				disabled
			>
				<DownloadIcon />
				Download PDF
			</button>
		</div>
	) : null
}

CodificationEditorFooter.propTypes = {
	showDownloadButton: PropTypes.bool.isRequired,
}

export default CodificationEditorFooter
