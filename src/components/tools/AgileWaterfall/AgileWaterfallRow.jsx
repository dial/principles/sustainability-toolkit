import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
} from '@mui/material'
import { networkStakeholderType } from '../../../types'
import {
	AGILE_EVALUATION_CATEGORY,
	SELF_NETWORK_STAKEHOLDER_KEY,
} from '../../../constants'
import agileWaterfallData from './agileWaterfallData'
import AgileEvaluationSelect from './AgileEvaluationSelect'
import styles from './AgileWaterfall.module.css'

function getLabelForEvaluationCategory(evaluationCategory) {
	switch (evaluationCategory) {
	case AGILE_EVALUATION_CATEGORY.ACCOUNTING:
		return agileWaterfallData.evaluationLabels.accounting
	case AGILE_EVALUATION_CATEGORY.CHANGE:
		return agileWaterfallData.evaluationLabels.change
	case AGILE_EVALUATION_CATEGORY.EXPERIMENTATION:
		return agileWaterfallData.evaluationLabels.experimentation
	case AGILE_EVALUATION_CATEGORY.ITERATION:
		return agileWaterfallData.evaluationLabels.iteration
	case AGILE_EVALUATION_CATEGORY.PROJECT_ACTIVITY_MANAGEMENT:
		return agileWaterfallData.evaluationLabels.projectActivityManagement
	case AGILE_EVALUATION_CATEGORY.PROJECT_PLANNING:
		return agileWaterfallData.evaluationLabels.projectPlanning
	default:
		return ''
	}
}

function getAriaLabelForEvaluationCategory(evaluationCategory) {
	switch (evaluationCategory) {
	case AGILE_EVALUATION_CATEGORY.ACCOUNTING:
		return agileWaterfallData.evaluationAriaLabels.accounting
	case AGILE_EVALUATION_CATEGORY.CHANGE:
		return agileWaterfallData.evaluationAriaLabels.change
	case AGILE_EVALUATION_CATEGORY.EXPERIMENTATION:
		return agileWaterfallData.evaluationAriaLabels.experimentation
	case AGILE_EVALUATION_CATEGORY.ITERATION:
		return agileWaterfallData.evaluationAriaLabels.iteration
	case AGILE_EVALUATION_CATEGORY.PROJECT_ACTIVITY_MANAGEMENT:
		return agileWaterfallData.evaluationAriaLabels.projectActivityManagement
	case AGILE_EVALUATION_CATEGORY.PROJECT_PLANNING:
		return agileWaterfallData.evaluationAriaLabels.projectPlanning
	default:
		return ''
	}
}

function AgileWaterfallHead({
	evaluationCategory,
	networkStakeholders,
	agileEvaluationsForEvaluationCategory,
	updateAgileEvaluation,
}) {
	const handleAgileEvaluationChange = (index, value) => {
		const networkStakeholderKey = (index === 0)
			? SELF_NETWORK_STAKEHOLDER_KEY
			: networkStakeholders[index - 1].key
		updateAgileEvaluation(
			evaluationCategory,
			networkStakeholderKey,
			value,
		)
	}

	return (
		<TableRow>
			<TableCell component="th">
				<div className={styles.agileCategoryLabel}>
					{ getLabelForEvaluationCategory(evaluationCategory) }
				</div>
			</TableCell>
			{ agileEvaluationsForEvaluationCategory.map((agileEvaluation, index) => (
				// We are truly mapping indices to these elements, and so array index IS the correct key
				// eslint-disable-next-line react/no-array-index-key
				<TableCell key={index}>
					<AgileEvaluationSelect
						aria-label={getAriaLabelForEvaluationCategory(evaluationCategory)}
						value={agileEvaluation}
						onChange={(event) => { handleAgileEvaluationChange(index, event.target.value) }}
					/>
				</TableCell>
			)) }
		</TableRow>
	)
}

AgileWaterfallHead.propTypes = {
	evaluationCategory: PropTypes.string.isRequired,
	networkStakeholders: PropTypes.arrayOf(networkStakeholderType).isRequired,
	agileEvaluationsForEvaluationCategory: PropTypes.arrayOf(PropTypes.string).isRequired,
	updateAgileEvaluation: PropTypes.func.isRequired,
}

export default AgileWaterfallHead
