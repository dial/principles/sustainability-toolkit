import React from 'react'
import { CANVAS_BLOCKS } from '../../constants'
import styles from './SustainabilityCanvas.module.css'
import SustainabilityCanvasBlock from './SustainabilityCanvasBlock'
import StickyEditModal from './StickyEditModal'

function SustainabilityCanvas() {
	return (
		<div className={styles.canvasWrapper}>
			<div className={styles.canvas}>
				{CANVAS_BLOCKS.map((block) => (
					<SustainabilityCanvasBlock
						key={block.slug}
						displayName={block.displayName}
						slug={block.slug}
					/>
				))}
			</div>
			<StickyEditModal />
		</div>
	)
}

export default SustainabilityCanvas
