export const CANVAS_BLOCKS = [
	{
		displayName: 'Value Proposition',
		slug: 'value-proposition',
	},
	{
		displayName: 'Customer Segments',
		slug: 'customer-segments',
	},
	{
		displayName: 'Channels',
		slug: 'channels',
	},
	{
		displayName: 'Customer Relationships',
		slug: 'customer-relationships',
	},
	{
		displayName: 'Revenue Streams',
		slug: 'revenue-streams',
	},
	{
		displayName: 'Key Activities',
		slug: 'key-activities',
	},
	{
		displayName: 'Key Resources',
		slug: 'key-resources',
	},
	{
		displayName: 'Key Partnerships',
		slug: 'key-partnerships',
	},
	{
		displayName: 'Cost Structure',
		slug: 'cost-structure',
	},
	{
		displayName: 'Organizational Development',
		slug: 'organizational-development',
	},
	{
		displayName: 'End Game',
		slug: 'end-game',
	},
]
