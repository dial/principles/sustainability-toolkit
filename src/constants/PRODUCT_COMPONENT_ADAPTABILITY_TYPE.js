export const PRODUCT_COMPONENT_ADAPTABILITY_TYPE = {
	CORE: 'Core',
	MODULAR: 'Modular',
	HACKABLE: 'Hackable',
	WRAPAROUND: 'Wraparound',
	NOT_PRESENT: 'Not Present',
}
