import { selectorFamily } from 'recoil'
import { generateGridOfValue } from '../../../utils'
import componentComparisonsAtom from './componentComparisonsAtom'

function generateComponentAndCompetitorStructure(
	componentComparisonsHashMap,
	productComponentOrderByKey,
	competitorOrderByKey,
) {
	const structuredSet = generateGridOfValue(
		competitorOrderByKey.length,
		productComponentOrderByKey.length,
		'',
	)

	Object.entries(componentComparisonsHashMap).forEach(
		(entry) => {
			const componentComparison = entry[1]
			const {
				productComponentKey,
				competitorKey,
				comparison,
			} = componentComparison
			const productComponentIndex = productComponentOrderByKey.indexOf(productComponentKey)
			const competitorIndex = competitorOrderByKey.indexOf(competitorKey)
			if (productComponentIndex !== -1 && competitorIndex !== -1) {
				structuredSet[productComponentIndex][competitorIndex] = comparison
			}
		},
	)

	return structuredSet
}

const SELECTOR_KEY = 'componentComparisonsThroughComponentAndCompetitorStructure'

const throughComponentAndCompetitorStructure = selectorFamily({
	key: SELECTOR_KEY,
	get: ({
		productComponentOrderByKey,
		competitorOrderByKey,
	}) => ({ get }) => {
		const componentComparisonsHashMap = get(componentComparisonsAtom)

		return generateComponentAndCompetitorStructure(
			componentComparisonsHashMap,
			productComponentOrderByKey,
			competitorOrderByKey,
		)
	},
})

export default throughComponentAndCompetitorStructure
