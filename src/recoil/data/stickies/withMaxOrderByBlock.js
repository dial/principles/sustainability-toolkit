import { selectorFamily } from 'recoil'
import { getLastInArray } from '../../../utils'
import withSortingByBlock from './withSortingByBlock'

const SELECTOR_KEY = 'maxOrderByBlock'

const withMaxOrderByBlock = selectorFamily({
	key: SELECTOR_KEY,
	get: (blockSlug) => async ({ get }) => {
		const stickies = get(withSortingByBlock(blockSlug))
		const lastSticky = getLastInArray(stickies)

		return Promise.resolve(lastSticky ? lastSticky.order : -1)
	},
})

export default withMaxOrderByBlock
