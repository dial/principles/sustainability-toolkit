import { selector } from 'recoil'
import { PRODUCT_COMPONENT_ADAPTABILITY_TYPE } from '../../../constants'
import throughNowFilter from './throughNowFilter'

function productComponentIsBlank(productComponent) {
	return productComponent.name === ''
}

const SELECTOR_KEY = 'productComponentsWithCodificationFilters'

const withCodificationFilters = selector({
	key: SELECTOR_KEY,
	get: ({ get }) => {
		const productComponents = get(throughNowFilter([
			PRODUCT_COMPONENT_ADAPTABILITY_TYPE.CORE,
			PRODUCT_COMPONENT_ADAPTABILITY_TYPE.MODULAR,
			PRODUCT_COMPONENT_ADAPTABILITY_TYPE.HACKABLE,
			'',
		]))

		return productComponents.filter(
			(productComponent) => !productComponentIsBlank(productComponent),
		)
	},
})

export default withCodificationFilters
