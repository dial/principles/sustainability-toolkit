import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { DEFAULT_NETWORK_STAKEHOLDERS_COUNT } from '../../../constants'
import { generateNetworkStakeholders } from '../../../components/tools/ValueNetwork/utils'

const ATOM_KEY = 'networkStakeholders'

const networkStakeholdersAtom = atom({
	key: ATOM_KEY,
	default: generateNetworkStakeholders(DEFAULT_NETWORK_STAKEHOLDERS_COUNT),
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default networkStakeholdersAtom
