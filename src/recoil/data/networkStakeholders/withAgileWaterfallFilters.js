import { selector } from 'recoil'
import { NETWORK_IDENTIFICATION } from '../../../constants'
import networkStakeholdersAtom from './networkStakeholdersAtom'

function labelIsBlank(networkStakeholder) {
	return networkStakeholder.label === ''
}

function networkIsIvn(networkStakeholder) {
	return networkStakeholder.networkIdentification === NETWORK_IDENTIFICATION.IVN
}

const SELECTOR_KEY = 'networkStakeholdersWithAgileWaterfallFilters'

const withoutBlanks = selector({
	key: SELECTOR_KEY,
	get: ({ get }) => {
		const networkStakeholders = get(networkStakeholdersAtom)

		return networkStakeholders.filter(
			(networkStakeholder) => !labelIsBlank(networkStakeholder)
				&& networkIsIvn(networkStakeholder),
		)
	},
})

export default withoutBlanks
