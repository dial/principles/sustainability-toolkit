import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { DEFAULT_PRODUCT_COMPONENT_COUNT } from '../../../constants'

const ATOM_KEY = 'nextProductComponentId'

const nextProductComponentIdAtom = atom({
	key: ATOM_KEY,
	default: DEFAULT_PRODUCT_COMPONENT_COUNT + 1,
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default nextProductComponentIdAtom
