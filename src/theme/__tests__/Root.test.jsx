import React from 'react'
import renderer from 'react-test-renderer'
import Root from '../Root'

test('Renders passed child elements', () => {
	const component = renderer.create(
		<Root>
			<h1>This is a child element</h1>
			<p>...so is this</p>
		</Root>,
	)
	const tree = component.toJSON()
	expect(tree).toMatchSnapshot()
})
