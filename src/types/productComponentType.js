import PropTypes from 'prop-types'

export const productComponentType = PropTypes.shape({
	key: PropTypes.string,
	id: PropTypes.number,
	name: PropTypes.string,
	now: PropTypes.string,
	future: PropTypes.string,
	actions: PropTypes.string,
	currentAutomationLevel: PropTypes.string,
	targetAutomationLevel: PropTypes.string,
	automationLevelTimeline: PropTypes.string,
})
