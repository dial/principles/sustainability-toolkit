# Business Model Sustainability Toolkit

![Pipeline Status](https://gitlab.com/dial/principles/sustainability-toolkit/badges/development/pipeline.svg)

This project is built using [Docusaurus](https://docusaurus.io/) and backed by the [Sustainability Toolkit API](https://gitlab.com/dial/principles/sustainability-toolkit-api), which powers the ability to save work done on the interative portions of pages.

## ❗️ Important note

 The preferred method of setting up the Toolkit complete with the API is using the instructions provided in the [Sustainability Toolkit Development Environment](https://gitlab.com/dial/principles/sustainability-toolkit-devenv) project. We strongly encourage you to use that repository as your setup entry point. (Note that there are [special instructions](https://gitlab.com/dial/principles/sustainability-toolkit-devenv/-/tree/main#development-on-arm-based-hardware) for developers on ARM-based hardware such as Apple Silicon-based Macs.)

## Local setup

Developing this project requires [yarn](https://yarnpkg.com/) to manage packages.

### Node version

The currently-targeted **production** version of Node is noted in `package.json` in the `"engines"` section.

The currently-targeted **development** version of Node is located in `.node-version`.  We recommend [nvm](https://github.com/nvm-sh/nvm) to manage your version of Node.

```bash
ln -s .node-version .nvmrc
nvm use
```

### Dependencies

To install dependencies:

```bash
yarn install
```

### Configuration

To configure the project with environment-specific options, copy the `.env.example` files to `.env` and populate its values. Or, if you have another preferred way of managing environment variables, you can just make sure the environment variables defined in `.env` are exported.

```bash
cp .env.example .env
edit .env
```

⚠️ Never commit your populated environment variables to the repository!

### Starting the server

To run the development server:

```bash
yarn start
```

The application should be visible at [http://localhost:3000](http://localhost:3000).

### Running the API server

If you plan to test API-backed features like user login, you will need to setup the [Sustainability Toolkit API](https://gitlab.com/dial/principles/sustainability-toolkit-api) and run both the Toolkit and API behind a reverse-proxy HTTP server, as described in the [Sustainability Toolkit Development Environment](https://gitlab.com/dial/principles/sustainability-toolkit-devenv) project.

## Project structure

```
- docs // magic folder Docusaurus uses for generating documentation-style content
- meta // documentation files for project developers (since `docs/` is taken)
- src // React/JavaScript code and components
|- pages // non-Guide pages like welcome and canvas and etc.
- static // static files that need to be served
```

## Running tests

We use Jest for automated tests; code contributions should come with relevant tests and tests should be run before opening a Merge Request.

```
yarn test
```

## License

See [LICENSE](./LICENSE) for the terms governing this software, and [docs/guide/LICENSE](./docs/guide/LICENSE) for the terms governing the Guide content.
